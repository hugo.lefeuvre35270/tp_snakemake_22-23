# TP Snakemake 22-23

## Project objectives 
The objective of this project is to re-analyse ATAC-seq (A methode for Assayin Chromatin Accessibility Genome-Wide) data to identify DNA accessible regions during transcription, using Snakemake, a tool to create reproducible and scalable data analyses with Python based language. We've done it on a virtual machine launched on the French Bioinformatics Institute (IFB).

The ATAC-seq data was produced from publication of David Gomez-Cabrero et al. 2019 https://doi.org/10.1038/s41597-019-0202-7. The data was collected by Nadia Goué with enaDataGet tool https://www.ebi.ac.uk/about/news/service-news/new-tools-download-data-ena, from https://www.ebi.ac.uk/ena, the date of this extraction: 2021-07-26

## Experimental Design  

- Cells collected for 0 and 24hours post-treatment with tamoxifen 
- 3 biological replicates of ~50,000 cells 
- Paired end sequencing using Illumina technology with Nextera-based sequencing primers
- Reference genome : Mus_musculus_GRCm39

## Raw dataset:

- SRR4785152  50k-R1-0h-sample.0h     GSM2367179   0.7G
- SRR4785153  50k-R2-0h-sample.0h     GSM2367180   0.7G
- SRR4785154  50k-R3-0h-sample.0h     GSM2367181   0.7G
- SRR4785341  50k-R1-24h-sample.24h   GSM2367368   0.6G
- SRR4785342  50k-R2-24h-sample.24h   GSM2367369   0.7G
- SRR4785343  50k-R3-24h-sample.24h   GSM2367370   0.6G

## Workflow
### Creating the working environment
I used a virtual machine (VM) created on the website of the French Institute of Bioinformatics (IFB) with Biosphere-commons app BioPipes, which can provides usual bioinformatics pipeline tools, including Snakemake. This VM consists of 4 CPUs, 15 GB of memory and 25 GB of storage but you can add more capabilities to perform workflow on raw data.

Access to the VM works with the use of a public ssh key and the conda environment with snakemake must be activated using the "conda snakemake activate" command.


The following directories must then be created: "data", "results", "scripts", "logs", "opt"
### Data
In the data directory, it is necessary to create a "raw" or "subset" directory corresponding to the data you want to process.
In this directory you can copy the ATAC-seq data you want to analyse. Please note that I used subset data named for example ss_50k_0h_R1_1.fastq.gz to test my workflow and as a result, you may have to change the name in the scripts depending on what you are going to process.

You must also create a "reference" directory.

In this one, it will be necessary to copy the files of the reference genome of the mouse for bowtie2 for the mapping.

### Results
In the results directory, the results of each workflow step will be stored in a corresponding directory.

### Scripts
In the scripts directory, you have to use the scripts in this Git project.

config.yaml :
In the config directory, you will find the config.yaml file, it will list all the transformation rules we want to implement in the workflow. In our case, it will be used to list the beginnings of file names so that a single rule can be used for all files using wildcards. In addition, it is used for reference data. This file can be modified according to the name of your files.

envs.yaml :
In the envs directory, you will find all the envs.yaml files corresponding to the creation of specific environments for each tool. It contains the Conda package manager which controls the automatic installation of the tool and the requested version as well as the required versions of the tools, and the dependencies necessary for their proper functioning. It allows to build the most reproducible workflow possible

Snakefile :
The snakefile is the file containing all the rules specific to the different tools we use. 

At the beginning, we find the path leading to the config file in order to use it.

Then there is the rule_all, which contains the output of all the rules, and which is necessary for snakemake to work properly.

Finally there are the rules for each tool, containing :
- File entries
- Output files
- The path to the corresponding env file
- The number of threads to be used
- The path to create the log file
- The message to display when the rule is launched
- The instructions that the rule must apply

### Logs
The logs directory contains the log files created during each rule and in which we can find information about the functioning of the tools.

### Opt
The opt directory, it contains the jar files necessary for the operation of the picard and trimmomatic tools. It is necessary to copy them both in the same apps directory for the good functioning of the workflow.

## Launching the snakefile
The snakefile must be launched at the base of the different directories, with the command "snakemake" followed by the options "--cores all", "use-conda", "--snakefile scripts/Snakefile".

Complete command :
snakemake --cores all --use-conda --snakefile scripts/Snakefile

It is recommended to use the --dry-run option to ensure that the command works with the correct accessions.
